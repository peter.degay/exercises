package first;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuickSort {

	private static List<Integer> sort(List<Integer> list) {		
		if (list.size() <= 1) {
			return list;
		}
		else {
			int half = list.size() / 2;
			return concat(sort(list.subList(0, half)), sort(list.subList(half, list.size())));
		}
	}
	
	/**
	 * This method gets two sorted lists, and concatenates them into a third which will be also sorted.
	 * The input lists must be sorted in an ascendant order.
	 * 
	 * @param list1 - sorted list no.1
	 * @param list2 - sorted list no.2
	 * @return a sorted list which contains all the elements of list1 and list2 in an ascendant order. 
	 */
	private static List<Integer> concat(List<Integer> list1, List<Integer> list2) {
		
		List<Integer> newList = new ArrayList<Integer>();
		int index1 = 0;
		int index2 = 0;
		Integer nextElementToAdd = 0;
		
		while (index1<list1.size() && index2 < list2.size()) {			
			if (list1.get(index1)<list2.get(index2)) {
				nextElementToAdd = list1.get(index1);
				index1++;
			} else {
				nextElementToAdd = list2.get(index2);
				index2++;				
			}
			newList.add(nextElementToAdd);
		}
		
		if (index1 == list1.size()) {
			newList.addAll(list2.subList(index2, list2.size()));
		} else {
			newList.addAll(list1.subList(index1, list1.size()));
		}
		
		return newList;
	}
	
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		int input;		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Enter numbers, 0 when done: ");
		
		input = in.nextInt();
		while (input != 0) {						
			list.add(input);
			input = in.nextInt();
		}
		in.close();
		
		System.out.println("The numbers in ascending order: " + sort(list));
	}
}