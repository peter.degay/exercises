	package first;

	import java.util.ArrayList;
	import java.util.List;
	import java.util.Scanner;

	public class MaxRecursive {
		
		private static int maxRecursive(List<Integer> list) {
			if (list.size() == 1) {
				return list.get(0);
			}
			else {
				int restMax = maxRecursive(list.subList(1, list.size()-1));
				return list.get(0) > restMax ? list.get(0) : restMax;
			}		
		}
		
		
		public static void main(String[] args) {
			ArrayList<Integer> list = new ArrayList<Integer>();
			int input;		
			Scanner in = new Scanner(System.in);
			System.out.println("Enter numbers, 0 when done: ");
			do {
				input = in.nextInt();			
				list.add(input);
				
			} while (input != 0);
			System.out.println("The maximum is: " + maxRecursive(list));
		}

	}